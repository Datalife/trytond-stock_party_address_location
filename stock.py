# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['ShipmentOut', 'ShipmentOutReturn', 'ShipmentIn',
    'ShipmentInReturn']


class ShipmentOutMixin(object):

    @fields.depends('delivery_address')
    def on_change_customer(self):
        super(ShipmentOutMixin, self).on_change_customer()
        if self.delivery_address:
            self.on_change_delivery_address()

    @fields.depends('customer', 'delivery_address')
    def on_change_delivery_address(self):
        if self.delivery_address:
            self.customer_location = self.on_change_with_customer_location()

    @fields.depends('customer', 'delivery_address')
    def on_change_with_customer_location(self, name=None):
        if self.delivery_address:
            return self.delivery_address.customer_location.id
        return super(ShipmentOutMixin, self).on_change_with_customer_location(
            name=name)


class ShipmentOut(ShipmentOutMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'


class ShipmentOutReturn(ShipmentOutMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'


class ShipmentIn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    delivery_address = fields.Many2One('party.address', 'Delivery Address',
        states={
            'readonly': Eval('state') != 'draft',
        },
        domain=[('party', '=', Eval('supplier'))],
        depends=['state', 'supplier'])

    @fields.depends('supplier')
    def on_change_supplier(self):
        if self.supplier:
            self.delivery_address = self.supplier.address_get('delivery')
            self.on_change_delivery_address()

    @fields.depends('supplier', 'delivery_address')
    def on_change_with_supplier_location(self, name=None):
        if self.delivery_address:
            return self.delivery_address.supplier_location.id
        return super(ShipmentIn, self).on_change_with_supplier_location(
            name=name)

    @fields.depends('delivery_address')
    def on_change_delivery_address(self):
        if self.delivery_address:
            self.supplier_location = self.on_change_with_supplier_location()


class ShipmentInReturn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return'

    @fields.depends('delivery_address')
    def on_change_supplier(self):
        if self.delivery_address:
            self.on_change_delivery_address()

    @fields.depends('delivery_address', 'to_location')
    def on_change_delivery_address(self):
        if not self.delivery_address:
            self.to_location = None
        elif not self.to_location:
            self.to_location = self.delivery_address.supplier_location
