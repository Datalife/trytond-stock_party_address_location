# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta

__all__ = ['SaleLine']


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    def get_from_location(self, name):
        if (self.quantity or 0) >= 0:
            if self.warehouse:
                return self.warehouse.output_location.id
        else:
            return self.sale.shipment_address.customer_location.id

    def get_to_location(self, name):
        if (self.quantity or 0) >= 0:
            return self.sale.shipment_address.customer_location.id
        else:
            if self.warehouse:
                return self.warehouse.input_location.id
