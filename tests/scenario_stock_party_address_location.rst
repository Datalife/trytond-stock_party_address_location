============================
Stock Party Address Scenario
============================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)

Install stock_party_address_location::

    >>> config = activate_modules('stock_party_address_location')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer 1')
    >>> customer.save()
    >>> supplier = Party(name='Supplier 1')
    >>> supplier.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])

Create custom stock locations::

    >>> cust_loc1 = Location(type='customer', name='Customer 1 loc.')
    >>> cust_loc1.save()
    >>> supp_loc1 = Location(type='supplier', name='Supplier 1 loc.')
    >>> supp_loc1.save()
    >>> cust_address, = customer.addresses
    >>> cust_address.customer_location = cust_loc1
    >>> cust_address.save()
    >>> supp_address, = supplier.addresses
    >>> supp_address.supplier_location = supp_loc1
    >>> supp_address.save()

Create Shipment In::

    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment_in = ShipmentIn()
    >>> shipment_in.supplier = supplier
    >>> shipment_in.warehouse = warehouse_loc
    >>> shipment_in.company = company
    >>> shipment_in.delivery_address == supp_address
    True
    >>> shipment_in.supplier_location == supp_loc1
    True
    >>> move = shipment_in.incoming_moves.new()
    >>> move.product = product
    >>> move.uom = unit
    >>> move.quantity = 100
    >>> move.from_location = supp_loc1
    >>> move.to_location = storage_loc
    >>> shipment_in.save()
    >>> shipment_in.click('receive')
    >>> shipment_in.click('done')

Create Shipment Out::

    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.planned_date = today
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company
    >>> shipment_out.delivery_address = cust_address
    >>> shipment_out.customer_location == cust_loc1
    True
    >>> move = shipment_out.outgoing_moves.new()
    >>> move.from_location = storage_loc
    >>> move.to_location = cust_loc1
    >>> move.product = product
    >>> move.uom = unit
    >>> move.quantity = 20
    >>> move.company = company
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> shipment_out.save()
    >>> shipment_out.click('wait')
    >>> shipment_out.click('assign_try')
    True
    >>> shipment_out.click('pack')
    >>> shipment_out.click('done')

Create Shipment Out Return::

    >>> ShipmentOutReturn = Model.get('stock.shipment.out.return')
    >>> shipment = ShipmentOutReturn()
    >>> shipment.customer = customer
    >>> shipment.warehouse = warehouse_loc
    >>> shipment.company = company
    >>> shipment.delivery_address = cust_address
    >>> shipment.customer_location == cust_loc1
    True
    >>> move = shipment.incoming_moves.new()
    >>> move.product = product
    >>> move.uom = unit
    >>> move.quantity = 5
    >>> move.from_location = cust_loc1
    >>> move.to_location = storage_loc
    >>> shipment.save()
    >>> shipment.click('receive')
    >>> shipment.click('done')