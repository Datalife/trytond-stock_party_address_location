# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, fields, ValueMixin, MultiValueMixin
from trytond.pool import PoolMeta, Pool

__all__ = ['Address', 'AddressLocation']


supplier_location = fields.Many2One(
    'stock.location', "Supplier Location", domain=[('type', '=', 'supplier')],
    help='The default source location when receiving products from the '
         'party address.')
customer_location = fields.Many2One(
    'stock.location', "Customer Location", domain=[('type', '=', 'customer')],
    help='The default destination location when sending products to the '
         'party address.'
    )


class Address(MultiValueMixin, metaclass=PoolMeta):
    __name__ = 'party.address'

    supplier_location = fields.MultiValue(supplier_location)
    customer_location = fields.MultiValue(customer_location)
    locations = fields.One2Many(
        'party.address.location', 'address', "Locations")

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in {'supplier_location', 'customer_location'}:
            return pool.get('party.address.location')
        return super(Address, cls).multivalue_model(field)

    @classmethod
    def default_supplier_location(cls, **pattern):
        return cls.multivalue_model(
            'supplier_location').default_supplier_location()

    @classmethod
    def default_customer_location(cls, **pattern):
        return cls.multivalue_model(
            'customer_location').default_customer_location()


class AddressLocation(ModelSQL, ValueMixin):
    "Party Address Location"
    __name__ = 'party.address.location'

    address = fields.Many2One(
        'party.address', "Address", ondelete='CASCADE', select=True)
    supplier_location = supplier_location
    customer_location = customer_location

    @classmethod
    def default_supplier_location(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('stock', 'location_supplier')
        except KeyError:
            return None

    @classmethod
    def default_customer_location(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('stock', 'location_customer')
        except KeyError:
            return None
