datalife_stock_party_address_location
============================

The stock_party_address_location module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_party_address_location/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_party_address_location)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
