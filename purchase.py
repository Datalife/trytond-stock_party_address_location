# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['Purchase', 'PurchaseLine']


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    shipment_address = fields.Many2One('party.address', 'Shipment Address',
        domain=[
            ('party', '=', Eval('party')),
            ],
        states={
            'readonly': Eval('state') != 'draft',
            'required': ~Eval('state').in_(['draft', 'quotation', 'cancelled']),
            },
        depends=['party', 'state'])

    def on_change_party(self):
        super(Purchase, self).on_change_party()
        if self.party:
            self.shipment_address = self.party.address_get(type='delivery')

    def _get_return_shipment(self):
        shipment = super(Purchase, self)._get_return_shipment()
        shipment.to_location = self.shipment_address.supplier_location
        shipment.delivery_address = self.shipment_address
        return shipment

    def _get_shipment_purchase(self, Shipment):
        shipment = super(Purchase, self)._get_shipment_purchase(Shipment)
        shipment.delivery_address = self.shipment_address
        return shipment


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    def get_from_location(self, name):
        if (self.quantity or 0) >= 0:
            return self.purchase.shipment_address.supplier_location.id
        elif self.purchase.return_from_location:
            return self.purchase.return_from_location.id

    def get_to_location(self, name):
        if (self.quantity or 0) >= 0:
            if self.purchase.warehouse:
                return self.purchase.warehouse.input_location.id
        else:
            return self.purchase.shipment_address.supplier_location.id
